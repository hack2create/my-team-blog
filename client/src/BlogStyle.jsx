import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    AppBar: {
        // marginTop: '20px'
        // height: '100px'
        // backgroundColor: 'hsl(180, 100%, 95%)'
        position: "sticky",
        top: "0"
    },
    toolBar: {
        display: 'flex',
        justifyContent: 'space-around',
        backgroundColor: '#ffffff'
    },

    toolBarTitle: {
        color: '#000000',
        // '&:hover': {
        //     background: "#4287f5"
        // }
    },

    blogBriefsContainer: {
        display: "flex",
        alignItems: "center",
        flexDirection: "column"
    },
    
    blogBrief: {
        marginTop: "50px"
    },

    blogBriefContent: {
        display: 'flex',
        // alignItems: 'center',
        flexDirection: 'column',
    },

    blogBriefTitle: {
        display: 'flex',
        justifyContent: 'center',
        fontFamily: "Rubik",
        fontSize: "56px",
        fontWeight: "700",
        color: '#000000'
    },

    blogDetailContainer: {
        marginTop: '50px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },

    blogTitile: {
        display: 'flex',
        justifyContent: 'center',
        fontFamily: "Rubik",
        fontSize: "56px",
        fontWeight: "700"
    },  

    blogContent: {
    },

    headerImg: props => ({
        display: 'flex',
        flexFlow: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundRepeat: "no-repeat",
        backgroundSize: "100% 100%",
        height: "1080px",
        opacity: 1/props.opacity,
        backgroundImage: 'url("/static/sky_night.png")',
        // width: "100%"
    }),

    slogan: {
        display: "flex",
        justifyContent: "center",
        textAlign: "center",
        width: "624px",
        height: "117px",
        fontFamily: "'Roboto Mono', 'monospace'",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: "37px",
        lineHeight: "39px",
        color: "#FCFBFB"
    },

    aboutButton: {
        boxShadow: "0px 4px 4px #000000",
        fontSize: "16px",
        lineHeight: "19px",
        backgroundColor: "rgba(8, 159, 244, 1)",
        width: "98px",
        marginTop: "37px"
    },

    searchInput: {
        marginTop: "253px",
        // color: "#FFFFFF",
        height: "45.85px",
        width: "336.52px",
        background: "rgba(67, 108, 214, 0.65)",
        input: {
            '&:placeholder': {
                color: "#FFFFFF"
            }
        }
    }
})

const useStylesBlogDetailPage = makeStyles({

})

export default useStyles