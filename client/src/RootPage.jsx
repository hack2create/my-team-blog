import React, { useState, useEffect, useContext, useRef } from 'react';
// import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Link from '@material-ui/core/Link';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Input } from '@material-ui/core';

import ReactHtmlParser from 'react-html-parser';

import OurAppBar from './OurAppBar';
import About from './About'
import Blog from './Blog'
import useStyles from './BlogStyle'

const RootPage = () => {
    const [transparentHeaderLevel, setStransparentHeaderLevel] = useState(1)
    const [blogs, setBlogs] = useState([])
    const classes = useStyles({opacity: transparentHeaderLevel});
    
    const renderBlogBriefs = () => {
        return blogs.map(blog=>{
            return (
                    <div className={classes.blogBrief}>
                        <Typography  className={classes.blogBriefTitle}>
                            <Link href={`/DetailContent?date=${blog.date}`}>
                                {ReactHtmlParser(blog.title)}
                            </Link>
                        </Typography>
                        <div className={classes.blogBriefContent}>
                             {ReactHtmlParser(blog.brief)}
                             {/* what is this */}
                        </div>
                    </div>       
                )
        })
    }

    useEffect(()=>{
        window.addEventListener('scroll', (e)=>{
            if(window.scrollY > 1) {
                setStransparentHeaderLevel(window.scrollY/100)
            } else {
                setStransparentHeaderLevel(1)
            }
        })

        async function fetchData() {
            let newestBlog = null
            newestBlog = await fetch(`/api/getLatestBlog`)
                                        .then(r => (r.json()))
                                        .then(data => (data))

            console.log("newestBlog: ", newestBlog)        
            let _blogs = await fetch(`/api/getBlogsBeforeDate?date=${newestBlog.date}`)
                                    .then(r=>(r.json()))
                                    .then(blogs=> (setBlogs(blogs)))
        }

        fetchData()

        return () => {

        }
    },[])

    const renderHeader = (    
            <div>
                <head>
                    <title>My Blog</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    {/* <link href="https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap" rel="stylesheet"></link> */}
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                </head>
                <div className={classes.headerImg}>          
                    <div className={classes.slogan}>
                        Tôi chỉ là một người luôn yêu<br/>
                            công việc mình<br/>
                                đang làm
                    </div>
                    <Button
                        variant="contained" 
                        style={{
                            boxShadow: "0px 4px 4px #000000",
                            fontSize: "16px",
                            lineHeight: "19px",
                            backgroundColor: "#089FF4",
                            width: "98px",
                            marginTop: "37px"
                        }}
                        onClick={()=>{document.location.href='/about'}}
                        >
                            About
                    </Button>
                    <Input 
                        autoComplete="first second" 
                        placeholder="Search..."
                        className={classes.searchInput}>
                    </Input>              
                </div>
            </div>
      )

      const  renderRootPage = () => {
          if(window.location.pathname === '/') {
              return (
                <>
                    {renderHeader}
                    <OurAppBar />
                    <div className={classes.blogBriefsContainer}>
                        {renderBlogBriefs()}
                    </div>
                </>
            )
          }
      }
    
    return (
        <>
            {renderRootPage()}
        </>
    )
}

export default RootPage