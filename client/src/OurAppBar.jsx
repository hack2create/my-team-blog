import React, { useState, useEffect, useContext, useRef } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import useStyles from "./BlogStyle"

const OurAppBar  = () => {
    const classes = useStyles()
    return (
         <AppBar color="default" className={classes.AppBar}>
                    <Toolbar className={classes.toolBar}>
                        <Typography variant="h6" className={classes.toolBarTitle}>
                            <Link href='/'>
                                The Interesting Things
                            </Link>    
                        </Typography>                    
                        
                        <Typography variant="h6" className={classes.toolBarTitle}>
                            <Link href="/myProjects">
                                My Projects
                            </Link>
                        </Typography>
                       
                        <Typography variant="h6" className={classes.toolBarTitle}>
                            <Link href="/about">
                                Let's Coffe  :)
                            </Link>
                        </Typography>
                </Toolbar>
        </AppBar>
    )
}

export default OurAppBar